import cStringIO as StringIO
import csv
import datetime
import gzip
import hashlib
import json
import logging
import os
import urllib2

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import psycopg2
import web

# TODO add IPN

urls = (
    r'/', 'Main',
    r'/data(.*)', 'Out',
    r'/hash', 'Hash',
)

app = web.application(urls, globals())

HOME_IP = '76.91.241.220'
HOST = 'ec2-54-235-147-211.compute-1.amazonaws.com'
USER = 'zlkmmlvyqrvdfx'
PASSWORD = '' # FIXME: add password in to make this live
DB_NAME = 'df2dg6o94unv2v'
HASH_LENGTH = len(hashlib.sha1('a').hexdigest())

def log(metadata_, app_id):
    conn = psycopg2.connect(user=USER,
                            host=HOST,
                            database=DB_NAME,
                            password=PASSWORD,
                            sslmode='require')
    c = conn.cursor()
    try:
        metadata = json.dumps(metadata_)
    except TypeError, e:
        conn.rollback()
        c.close()
        raise web.HTTPError(404, 'metadata must be JSON')
    try:
        if len(app_id) != HASH_LENGTH:
            app_id = hashlib.sha1(app_id).hexdigest()
    except UnboundLocalError, e:
        conn.rollback()
        c.close()

    c.execute("INSERT INTO applicationlogs(app_id, metadata) VALUES (%s, %s)", (app_id, metadata))
    c.close()
    conn.commit()


class Hash:
    def POST(self):
        string = web.input()['q']
        ret = StringIO.StringIO()
        stanza = {'hash': {'sha1': hashlib.sha1(string).hexdigest(),
                           'sha224': hashlib.sha224(string).hexdigest(),
                           'md5': hashlib.md5(string).hexdigest(),
                           'sha512': hashlib.sha512(string).hexdigest()}
        }
        compressed_stanza = gzip.GzipFile(fileobj=ret, mode='w')
        json_out = json.dumps(stanza)
        compressed_stanza.write(json_out)
        compressed_stanza.close()
        web.header('Content-Encoding', 'gzip')
        web.header('Content-Type', 'application/json')
        web.ctx.status = '200 OK'
        return ret.getvalue()

    def GET(self):
        return self.POST()


class Out:
    def GET(self, data_format):
        form = data_format[1:]
        conn = psycopg2.connect(user=USER, host=HOST, database=DB_NAME, password=PASSWORD, sslmode='require')
        c = conn.cursor()
        incoming_param = web.input()
        
        data_to_log = {'endpoint': 'GET /data.{0}'.format(form)}
        c = conn.cursor()
        if not incoming_param.has_key('app_id'):        
            c.execute("SELECT app_id from applicationlogs group by app_id order by count(app_id)")
            results = c.fetchall()[-1]
            incoming_param['app_id'] = results[0]
        app_id = incoming_param['app_id']
        logging.debug('App id: {0}'.format(app_id))
        logging.debug("HASH_LENGTH = {0}, length of passed in app_id is {1}".format(HASH_LENGTH, len(app_id)))
        if len(incoming_param['app_id']) == HASH_LENGTH:
            frame = pd.read_sql_query("select app_id as application, read_on as timestamp, to_json(metadata) as metadata from applicationlogs where app_id = %s", conn, params=[incoming_param['app_id']])
        else:
            frame = pd.read_sql_query("SELECT encode(digest(app_id, 'sha1'), 'hex') as application, read_on as timestamp, to_json(metadata) as metadata from applicationlogs where app_id = %s", conn, params=[incoming_param['app_id']])
        frame = frame.reindex(index=frame.index[::-1])
        log(data_to_log, "logging")

        pd.set_option('display.float_format', lambda x: '%.2f')
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')
        if form == 'json':
            web.header('Content-Type', 'application/json')
            returnVal_raw = frame.to_json(force_ascii=False, orient='split', double_precision=2)
            returnVal = json.loads(returnVal_raw)
            ret = json.dumps(returnVal['data'])

        elif form == 'latex' or form == 'tex':
            web.header('Content-Type', 'application/latex')
            ret = r'''\document{article}\begin{document}'''+frame.to_latex()+r'\end{article}\end{document}'

        elif form == 'html':
            web.header('Content-Type', 'text/html')
            raise web.seeother('/static/sample.html')

        elif form == 'excel' or form == 'xls' or form == 'xlsx':
            frame.to_excel('{0}.xls'.format(incoming_param['app_id']), 'Logging data', na_rep='not supplied')
            with open('{0}.xls'.format(incoming_param['app_id'])) as fin:
                web.header('Content-Type', 'application/vnd.ms-excel')
                ret = fin.read()
            os.unlink('{0}.xls')

        elif form == 'xml':
            web.header('Content-Type', 'text/xml')
            ret = self.to_xml(frame)

        elif form == 'csv':
            web.header('Content-Type', 'text/csv')
            ret = frame.to_csv(index=False, encoding='utf-8', float_format='%f', escapechar='\\', doublequote=True, quoting=csv.QUOTE_ALL)

        else:
            raise web.HTTPError(404, 'Must specify a valid format for data')

        returnValIO = StringIO.StringIO()
        returnValIO.write(ret)
        return returnValIO.getvalue()

    def to_xml(self, df):
        frame_as_dict = df.to_dict('records')
        ret = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<logging><records>"
        for row in frame_as_dict:
            ret = ret + '<record '
            for k in row.keys():
                v = unicode(row[k]).encode('utf-8').replace('"', "'").replace('\\', '')
                ret = ret + '{0}="{1}" '.format(k, v)
            ret = ret + "/>"
        ret = ret + '</records></logging>'
        return ret
        
class Main:
    def POST(self):
        """
        Two parameters:
        - app_id is the unhashed application name
        - metdata what data you want to store with the database hit
        """
        incoming_params = web.input()
        app_id = incoming_params['app_id']

        data_to_log = {'metadata': json.dumps(incoming_params)}
        log(data_to_log, app_id)

        web.header('X-Application-Id', incoming_params['app_id'])
        web.ctx.status = '201 created'
        if len(app_id) != 40:
            app_id = hashlib.sha1(incoming_params['app_id']).hexdigest()
        return json.dumps({"Application-Id": '{0}'.format(app_id)})

    def GET(self):
        out = Out()
        data_in_raw = out.GET('.json')
        logging.debug(data_in_raw)
        data_in = json.loads(data_in_raw)
        data = pd.DataFrame(data_in)
        hours = []
        for d in data[1]:
            incoming_date = d/1000
            logging.debug('timestamp stripped: {0}'.format(incoming_date))
            stamp_as_int = int(incoming_date)
            logging.debug('timestamp as int: {0}'.format(stamp_as_int))
            stamp_for_datetime = stamp_as_int/1000
            logging.debug("Python's notion of epoch time: {0}".format(stamp_for_datetime))
            stamp_as_datetime = datetime.datetime.fromtimestamp(stamp_for_datetime)
            logging.debug('datetime: {0}'.format(stamp_as_datetime))
            hour_as_string = stamp_as_datetime.strftime('%H')
            hour = int(hour_as_string)
            logging.debug('Hour of day: {0}'.format(hour))
            hours.append(hour)
        logging.debug(hours)
        hours_as_array = np.asarray(hours)

        plt.figure()
        logging.debug('matplotlib initialised!')

        plt.hist(hours_as_array, bins=range(0, 24))
        logging.debug('histogram set up!')

        plt.title('Hourly Hits on logging.d8u.us')
        logging.debug('title set!')

        plt.xlabel('Hour of Day (UTC/Zulu)')
        logging.debug('x-axis titled!')

        plt.xlim(0, 23)
        logging.debug('x-axis restricted!')

        plt.ylabel('Number of hits')
        logging.debug('y-axis titledt!')

        svg = StringIO.StringIO()
        logging.debug('SVG initialised!')

        plt.savefig(svg, format='svg')
        logging.debug('svg saved!')

        svg = svg.getvalue()
        logging.debug(svg)

        web.header('Content-Type', "image/svg+xml")
        return svg


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    conn = psycopg2.connect(user=USER, host=HOST, database=DB_NAME, password=PASSWORD, sslmode='require')
    c = conn.cursor()
    c.execute('CREATE TABLE IF NOT EXISTS applicationlogs (id serial primary key, read_on timestamp default now(), app_id citext not null, metadata text not null)')
    conn.commit()
    c.close()

    app.run()
